#%%
import pandas as pd

# 1.4.1
#データ読み込み
#df = pd.read_csv('http://www.minethatdata.com/Kevin_Hillstrom_MineThatData_E-MailAnalytics_DataMiningChallenge_2008.03.20.csv')
#df.to_feather('data\\Kevin_Hillstrom_MineThatData_E-MailAnalytics_DataMiningChallenge_2008.03.20.fth')

#%%
df = pd.read_feather('data\\Kevin_Hillstrom_MineThatData_E-MailAnalytics_DataMiningChallenge_2008.03.20.fth')
#%%
df.describe()

# %%
# "メールを送った男性"&"送らない"のセグメントだけ見る
male_df = df[df["segment"]!="Womens E-Mail"]
male_df["treatment"] = male_df["segment"].apply(lambda x: 1 if x=="Mens E-Mail" else 0)
male_df.head()

# %%
# 1.4.2
# メールを送った男性とメールを送らないのコンバージョン、費用、件数、費用の分散
agg_male_df = male_df.groupby(by="treatment").agg(coversion_rate=("conversion","mean"),
                                                  spend_mean=("spend","mean"),
                                                  spend_var=("spend","std"),
                                                  count=("treatment","count"))
agg_male_df

# %%
# 平均値のさに対して、有意差検定
# 
from statsmodels.stats.weightstats import ttest_ind

# t-検定(分散は同じと仮定しない。)
print(ttest_ind(male_df[male_df["treatment"]==1]["spend"], male_df[male_df["treatment"]==0]["spend"], usevar="unequal"))
# t-検定(分散は同じものとする。)
print(ttest_ind(male_df[male_df["treatment"]==1]["spend"], male_df[male_df["treatment"]==0]["spend"], usevar="pooled"))

#%%
# 1.4.3
# メールを配信していないグループ
#   昨年購入金額が300より高い場合 or 最後の購入であるrecencyが3より小さい or 接触チャンネルが複数持つ
# メールを配信したグループ
#   not (昨年購入金額が300より高い場合 or 最後の購入であるrecencyが3より小さい or 接触チャンネルが複数持つ)
# 購入意欲が高いユーザーにメールが配信されるようにバイアスをかけた。

sample_rules = (male_df.history > 300) | (male_df.recency < 6) | (male_df.channel=='Multichannel')
biased_male_df = pd.concat([male_df[(sample_rules) & (male_df.treatment == 0)].sample(frac=0.5, random_state=1),
                            male_df[(sample_rules) & (male_df.treatment == 1)],
                            male_df[(~sample_rules) & (male_df.treatment == 0)],
                            male_df[(~sample_rules) & (male_df.treatment == 1)].sample(frac=0.5, random_state=1)],
                           axis=0, ignore_index=True)

# %%
agg_biased_male_df = biased_male_df.groupby(by="treatment").agg(coversion_rate=("conversion","mean"),
                                                                spend_mean=("spend","mean"),
                                                                spend_var=("spend","std"),
                                                                count=("treatment","count"))
agg_biased_male_df

#%%
# t-検定(分散は同じと仮定しない。)
print(ttest_ind(biased_male_df[biased_male_df["treatment"]==1]["spend"], biased_male_df[biased_male_df["treatment"]==0]["spend"], usevar="unequal"))
# t-検定(分散は同じものとする。)
print(ttest_ind(biased_male_df[biased_male_df["treatment"]==1]["spend"], biased_male_df[biased_male_df["treatment"]==0]["spend"], usevar="pooled"))
# p valueなどさらに小さくなる。

# %%


